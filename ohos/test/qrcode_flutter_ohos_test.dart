import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  const MethodChannel methodChannel =
  MethodChannel('plugins/qr_capture/method');
  TestDefaultBinaryMessengerBinding.instance?.defaultBinaryMessenger
      .setMockMethodCallHandler(
    methodChannel,
        (message) => null,
  );

  test('getQrCodeByImagePath', () async {
    String qrResult = '';
    var res = await methodChannel.invokeMethod("getQrCodeByImagePath", '');
    if (res != null) {
      qrResult = res;
    }
    expect(qrResult, isNotNull);
  });
}
